# Configure a host

Follow the instructions on this page to configure a system as a KVM virtualization host (hypervisor) that is capable of running TD protected guests (VMs).

## Hardware Requirements

- [4th Generation Intel® Xeon® Scalable Processors](https://ark.intel.com/content/www/us/en/ark/products/series/228622/4thgeneration-intel-xeon-scalable-processors.html) or newer with Intel® TDX.


## Operating System

Download and install the latest CentOS Stream 9 x86_64 image from the [CentOS website](https://www.centos.org/centos-stream/).

## Packages

1. Add the Virt SIG TDX package repository to the system by running the command below which will create `/etc/yum.repos.d/tdx.repo`:

    ```bash
    echo "[tdx]
    name=CentOS TDX
    metalink=https://mirrors.centos.org/metalink?repo=centos-virt-sig-tdx-devel-\$releasever-stream&arch=\$basearch&protocol=https
    gpgcheck=0
    enabled=1" | sudo tee /etc/yum.repos.d/tdx.repo
    ```

    Alternatively, you can manually create the repo file yourself in `/etc/yum.repos.d/` with the content below:
    ```ini
    [tdx]
    name=CentOS TDX
    metalink=https://mirrors.centos.org/metalink?repo=centos-virt-sig-tdx-devel-$releasever-stream&arch=$basearch&protocol=https
    gpgcheck=0
    enabled=1
    ```

1. Install the TDX host packages:

    ```bash
    dnf install \
    kernel-tdx \
    qemu-kvm-tdx \
    libvirt-tdx
    ```

## UEFI (BIOS) settings

The system needs to have certain UEFI (BIOS) settings configured for TDX. Below are paths from Intel's reference UEFI.
Note: UEFI settings and locations may vary by vendor.

1. Enable Software Guard Extension (SGX)

    - Go to EDKII MENU -> Socket Configuration -> Processor Configuration -> Software Guard Extension (SGX)
        - Set SW Guard Extensions (SGX) to Enable


1. Enable TDX and TME/MKTME

    - Go to EDKII MENU -> Socket Configuration -> Processor Configuration -> TME, TME-MT, TDX
      - Set Total Memory Encryption (TME) to `Enable`
      - Set Total Memory Encryption Bypass to `Enable` (optional; for best host and non-TDVM performance)
      - Set Total Memory Encryption Multi-Tenant (TME-MT) to `Enable`
      - Set TME-MT memory integrity to `Disable`
      - Set Trust Domain Extension (TDX) to `Enable`
      - Set the TME-MT/TDX key split to a non zero value
      - Set TDX Secure Arbitration Mode Loader (SEAM Loader) to `Enable`. This allows loading SEAMLDR and TDX modules  from the ESP or BIOS.
      

## Verification

1. Reboot the system and verify the running kernel version matches the one provided in the Virt SIG TDX repo: 

    Print the kernel version being privded in the Virt SIG repository:
    ```bash
    dnf repository-packages tdx list kernel
    ```

    Print the name of the running kernel:
    ```bash
    uname -r 
    ```

1. Verify that TDX is enabled using the dmesg command:

    ```bash
    sudo dmesg | grep -i tdx
    ```

    Example output:
    ```
    [ 0.276580] tdx: BIOS enabled: private KeyID range [64, 128)
    ```

1. Reload kvm_intel module with tdx=1 to enable TDX in KVM:

    ```bash
    sudo rmmod kvm_intel
    sudo modprobe kvm_intel tdx=1
    ```

    To enable TDX by default at boot use one of the options below:

    - Option 1: Add `kvm_intel.tdx=1` into host kernel cmdline. For example: 
    ```bash
    sudo grubby --update-kernel=ALL --args=kvm_intel.tdx=1
    ``` 
    - Option 2: Add `options kvm_intel tdx=1` into a `/etc/modprobe.d/` drop-in file. For example: 
    ```bash
    echo "options kvm_intel tdx=1" | sudo tee /etc/modprobe.d/tdx.conf
    ```

1. Verify TDX is initialized on the host by checking for new output from the dmesg command:

    ```bash
    sudo dmesg | grep -i tdx
    ```

    Example output:
    ```
    [ 0.276580] tdx: BIOS enabled: private KeyID range [64, 128)
    [ 1171.427593] tdx: TDX module: attributes 0x0, vendor_id 0x8086, major_version 1, minor_version 5,  build_date 20230420, build_num 507
    [ 1171.427600] tdx: CMR: [0x100000, 0x77800000)
    [ 1171.427602] tdx: CMR: [0x100000000, 0x107a000000)
    [ 1171.427603] tdx: CMR: [0x1080000000, 0x207c000000)
    [ 1171.427604] tdx: CMR: [0x2080000000, 0x307c000000)
    [ 1171.427606] tdx: CMR: [0x3080000000, 0x407c000000)
    [ 1172.577580] tdx: 1050644 KBs allocated for PAMT.
    [ 1172.577589] tdx: module initialized.
    ```


The system is now configured as a KVM host capable of hosting and securing TD guests. See [Run a TDX guest](./guest.md) next.
